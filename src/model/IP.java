package model;

import blockparty.CellRange;
import blockparty.Puzzle;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.Status;
import java.util.Set;

/**
 * IP creates an integer programming model for a puzzle.
 *
 * Since this is a feasibility problem, we omit the objective function
 * (or equivalently minimize the constant value 0).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class IP implements AutoCloseable {
  private static final double HALF = 0.5;  // rounding cutoff
  private final Puzzle puzzle;             // the puzzle to solve

  // CPLEX objects
  private final IloCplex cplex;            // the model instance
  private final IloIntVar[][] x;           // x[i][j] = 1 if value j is
                                           // inserted in cell i

  /**
   * Constructor.
   * @param p the puzzle to solve
   * @throws IloException if the IP model cannot be built
   */
  public IP(final Puzzle p) throws IloException {
    puzzle = p;
    int nCells = puzzle.getnCells();
    int nBlocks = puzzle.getnBlocks();
    int maxBlockSize = puzzle.getMaxBlockSize();
    // Initialize the model instance.
    cplex = new IloCplex();
    // Create a variable for each cell and for each value from 0 to the size
    // of the parent block. To avoid undefined variables creeping into a
    // CPLEX function call, we define a variable for each cell index and
    // each value from 0 to the size of the largest block (inclusive) and then
    // fix variables corresponding to illegal values at 0 and fix variables
    // corresponding to any prespecified values at 1.
    x = new IloIntVar[nCells][maxBlockSize + 1];
    for (int i = 0; i < nCells; i++) {
      int mx = puzzle.getMaxValue(i);  // maximum legal value in the cell
      int fixed = puzzle.valueOf(i).orElse(0);  // fixed value if any, else 0
      for (int j = 0; j <= maxBlockSize; j++) {
        x[i][j] = cplex.boolVar("Assign_" + j + "_to_" + i);
        if (j == 0 || j > mx) {
          x[i][j].setUB(0);    // value is out of range
        } else if (fixed > 0) {
          if (j == fixed) {
            x[i][j].setLB(1);  // cell is fixed at this value
          } else {
            x[i][j].setUB(0);  // cell is fixed at some other value
          }
        }
      }
    }
    // Constraint: exactly one value must be selected for each cell.
    for (int i = 0; i < nCells; i++) {
      cplex.addEq(cplex.sum(x[i]), 1.0, "Set_value_" + i);
    }
    // The remaining constraints are implemented on a block-by-block basis.
    for (int b = 0; b < nBlocks; b++) {
      // Get the set of cells contained in the block and the upper limit on
      // values in the block.
      Set<Integer> block = puzzle.getBlock(b);
      int max = block.size();
      // Implement the constraints for each possible cell value in the block.
      for (int v = 1; v <= max; v++) {
        // Constraint: within each block, each feasible value must be used
        // exactly once.
        IloLinearNumExpr sum = cplex.linearNumExpr();
        for (int i : block) {
          sum.addTerm(1.0, x[i][v]);
        }
        cplex.addEq(sum, 1.0, "Value_" + v + "_block_" + b);
        // The next two constraints are implemented cell by cell.
        for (int i : block) {
          // Find out which cells are at distance v or less from cell i.
          CellRange range = puzzle.getCells(i, v);
          // Constraint: to use value v in cell i, there must be a cell at
          // distance v containing the value v.
          sum = cplex.linearNumExpr();
          for (int j : range.at()) {
            sum.addTerm(1.0, x[j][v]);
          }
          cplex.addLe(x[i][v], sum, "at_range_" + i + "_" + v);
          // Constraint: to use value v in cell i, no cell j at distance less
          // than v can contain v. We only need record these constraints
          // for the case i < j.
          for (int j : range.nearer()) {
            if (j > i) {
              cplex.addLe(cplex.sum(x[i][v], x[j][v]), 1.0,
                          "conflict_" + i + "_" + j + "_" + v);
            }
          }
        }
      }
    }
//    cplex.exportModel("/tmp/ip.lp");
  }

  /**
   * Closes the model instance.
   */
  @Override
  public void close() {
    cplex.close();
  }

  /**
   * Solves the model.
   * @param timelimit a time limit (in seconds) for the attempt
   * @return the final solver status
   * @throws IloException if anything goes wrong
   */
  public Status solve(final double timelimit) throws IloException {
    // Set the time limit.
    cplex.setParam(IloCplex.DoubleParam.TimeLimit, timelimit);
    // Try to solve the model.
    cplex.solve();
    // Return the final status.
    return cplex.getStatus();
  }

  /**
   * Gets the grid corresponding to the IP solution.
   * @return the solution grid
   * @throws IloException if the solution cannot be recovered
   */
  public int[][] getSolution() throws IloException {
    int nCells = puzzle.getnCells();
    int dim = puzzle.getDim();
    int[][] sol = new int[dim][dim];
    for (int i = 0; i < nCells; i++) {
      // Get the row and column of the cell.
      int[] rc = puzzle.getCell(i);
      boolean success = false;
      // Get the values of the variables for cell i.
      double[] vals = cplex.getValues(x[i]);
      // Find the one that has value 1.
      for (int j = 1; j < vals.length; j++) {
        if (vals[j] > HALF) {
          sol[rc[0]][rc[1]] = j;
          success = true;
          break;
        }
      }
      if (!success) {
        throw new IloException("No value for cell " + i + "?!");
      }
    }
    return sol;
  }
}
