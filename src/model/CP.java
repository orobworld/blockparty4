package model;

import blockparty.CellRange;
import blockparty.Puzzle;
import ilog.concert.IloConstraint;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.cp.IloCP;
import java.util.ArrayList;
import java.util.Set;

/**
 * CP implements a constraint programming model for a puzzle.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class CP implements AutoCloseable {
  private final Puzzle puzzle;        // the puzzle to solve

  // CPO objects
  private final IloCP cp;             // the model instance
  private final IloIntVar[] x;        // the value inserted into each cell

  /**
   * Constructor.
   * @param p the puzzle to solve
   * @throws IloException if the IP model cannot be built
   */
  public CP(final Puzzle p) throws IloException {
    puzzle = p;
    int nCells = puzzle.getnCells();
    int nBlocks = puzzle.getnBlocks();
    int maxBlockSize = puzzle.getMaxBlockSize();
    Set<Integer> fixed = puzzle.getFixedCells();
    // Initialize the model instance.
    cp = new IloCP();
    // Create the variable array.
    x = new IloIntVar[nCells];
    for (int i = 0; i < nCells; i++) {
      if (fixed.contains(i)) {
        int j = puzzle.valueOf(i).get();
        x[i] = cp.intVar(j, j, "x_" + i);
      } else {
        x[i] = cp.intVar(1, maxBlockSize, "x_" + i);
      }
    }
    // Proceed block by block.
    for (int b = 0; b < nBlocks; b++) {
      // Get the set of cells contained in the block and the upper limit on
      // values in the block.
      Set<Integer> block = puzzle.getBlock(b);
      int max = block.size();
      // Accumulate the cells in the block in a vector, adjusting their upper
      // bounds to the block size (unless they are fixed).
      ArrayList<IloIntVar> list = new ArrayList<>();
      for (int i : block) {
        if (!fixed.contains(i)) {
          x[i].setMax(max);
        }
        list.add(x[i]);
      }
      // The cells in the block must take distinct values.
      cp.add(cp.allDiff(list.toArray(IloIntVar[]::new)));
      // The next two constraints are implemented cell by cell.
      for (int i : block) {
        for (int v = 1; v <= max; v++) {
          // Find out which cells are at distance v or less from cell i.
          CellRange range = puzzle.getCells(i, v);
          // Constraint: to use value v in cell i, there must be a cell at
          // distance v containing the value v.
          IloConstraint[] cons = new IloConstraint[range.at().size()];
          int ix = 0;
          for (int j : range.at()) {
            cons[ix] = cp.eq(x[j], v);
            ix += 1;
          }
          cp.add(cp.imply(cp.eq(x[i], v), cp.or(cons)));
          // Constraint: to use value v in cell i, no cell j at distance less
          // than v can contain v. We only need record these constraints
          // for the case i < j.
          for (int j : range.nearer()) {
            if (j > i) {
              cp.add(cp.imply(cp.eq(x[i], v), cp.neq(x[j], v)));
            }
          }
        }
      }
    }
  }

  /**
   * Closes the model instance.
   */
  @Override
  public void close() {
    cp.end();
  }

  /**
   * Solves the model.
   * @param timelimit a time limit (in seconds) for the attempt
   * @return true if a solution is found
   * @throws IloException if anything goes wrong
   */
  public boolean solve(final double timelimit) throws IloException {
    cp.setParameter(IloCP.DoubleParam.TimeLimit, timelimit);
    return cp.solve();
  }

  /**
   * Gets the grid corresponding to the IP solution.
   * @return the solution grid
   * @throws IloException if the solution cannot be recovered
   */
  public int[][] getSolution() throws IloException {
    int nCells = puzzle.getnCells();
    int dim = puzzle.getDim();
    int[][] sol = new int[dim][dim];
    for (int i = 0; i < nCells; i++) {
      // Get the row and column of the cell.
      int[] rc = puzzle.getCell(i);
      // Get the value of the cell.
      sol[rc[0]][rc[1]] = cp.getValue("x_" + i);
    }
    return sol;
  }
}
