package blockparty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Puzzle houses the elements of the puzzle.
 *
 * Cells and blocks use zero-based indexing, but cell values use the domain
 * 1, ..., K where K is the size of the block containing the cell.
 *
 * Cells are assigned index values from 0 to N^2 - 1, where N is the number
 * of rows/columns.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Puzzle {
  private final int dim;           // number of rows/columns in the grid
  private final int nCells;        // number of cells in the puzzle
  private final int nBlocks;       // number of blocks
  private final int maxBlockSize;  // size of the largest block
  private final int[][] index;     // index[i][j] = index of the cell
                                   // in row i, col j
  private final int[][] cell;      // row and column of each cell
  private final int[][] distance;  // 1-norm distance between cells
  private final int[] blockIndex;  // index of block containing each cell
  private final ArrayList<HashSet<Integer>> block;  // cells in each block
  private final int[] blockSize;   // the size of each block
  private final int[] value;       // value of each cell if fixed, 0 if not
  private final HashSet<Integer> fixed;  // indices of fixed variables

  /**
   * Constructor.
   * @param blocks the number of blocks in the puzzle
   * @param blockID a matrix containing the block index of each cell
   * @param fixedVals a matrix of fixed values (row, column, value)
   */
  public Puzzle(final int blocks, final int[][] blockID,
                final int[][] fixedVals) {
    nBlocks = blocks;
    dim = blockID.length;
    nCells = dim * dim;
    index = new int[dim][dim];
    cell = new int[nCells][2];
    // Assign cell indices.
    int ix = 0;
    for (int r = 0; r < dim; r++) {
      for (int c = 0; c < dim; c++) {
        index[r][c] = ix;
        cell[ix][0] = r;
        cell[ix][1] = c;
        ix += 1;
      }
    }
    // Compute cell distances in "taxi cab" (1) norm.
    distance = new int[nCells][nCells];
    for (int c = 0; c < nCells; c++) {
      for (int c0 = c + 1; c0 < nCells; c0++) {
        int d = Math.abs(cell[c][0] - cell[c0][0])
                + Math.abs(cell[c][1] - cell[c0][1]);
        distance[c][c0] = d;
        distance[c0][c] = d;
      }
    }
    // Store the set of cells in each block and compute the size of each block
    // as well as the maximum block size. Also assign a block index to each
    // cell.
    blockSize = new int[nBlocks];
    blockIndex = new int[nCells];
    block = new ArrayList<>();
    for (int i = 0; i < nBlocks; i++) {
      block.add(new HashSet<>());
    }
    for (int r = 0; r < dim; r++) {
      for (int c = 0; c < dim; c++) {
        int bid = blockID[r][c];
        ix = index[r][c];
        blockSize[bid] += 1;
        block.get(bid).add(ix);
        blockIndex[ix] = bid;
      }
    }
    maxBlockSize = Arrays.stream(blockSize).max().getAsInt();
    // Record the fixed values.
    fixed = new HashSet<>();
    value = new int[nCells];
    for (int[] fixed1 : fixedVals) {
      int r = fixed1[0];
      int c = fixed1[1];
      ix = index[r][c];
      fixed.add(ix);
      value[ix] = fixed1[2];
    }
  }

  /**
   * Gets the dimension of the puzzle grid.
   * @return the puzzle dimension
   */
  public int getDim() {
    return dim;
  }

  /**
   * Gets the number of cells.
   * @return the number of cells
   */
  public int getnCells() {
    return nCells;
  }

  /**
   * Gets the number of blocks.
   * @return the number of blocks
   */
  public int getnBlocks() {
    return nBlocks;
  }

  /**
   * Gets the maximum block size.
   * @return the size of the largest block
   */
  public int getMaxBlockSize() {
    return maxBlockSize;
  }

  /**
   * Gets the block index of a cell.
   * @param c the index of the cell
   * @return the index of the block containing it
   */
  public int getBlockIndex(final int c) {
    return blockIndex[c];
  }

  /**
   * Gets the maximum value of a cell (the size of its parent block).
   * @param c the index of the cell
   * @return the maximum value it can contain
   */
  public int getMaxValue(final int c) {
    return blockSize[blockIndex[c]];
  }

  /**
   * Gets the value of a cell if fixed (empty if not fixed).
   * @param c the index of the cell
   * @return the value if fixed
   */
  public Optional<Integer> valueOf(final int c) {
    if (value[c] > 0) {
      return Optional.of(value[c]);
    } else {
      return Optional.empty();
    }
  }

  /**
   * Gets the set of indices of cells in a given block.
   * @param b the index of the target block
   * @return the set of cells in the block
   */
  public Set<Integer> getBlock(final int b) {
    return new HashSet<>(block.get(b));
  }

  /**
   * Gets the indices of cells at a given distance or less from a given cell.
   * @param target the target cell
   * @param dist the distance
   * @return the sets of cells at that distance or closer from the target
   */
  public CellRange getCells(final int target, final int dist) {
    HashSet<Integer> a = new HashSet<>();  // cells at the given distance
    HashSet<Integer> n = new HashSet<>();  // cells that are nearer
    for (int i = 0; i < nCells; i++) {
      // Skip the target cell.
      if (i == target) {
        continue;
      }
      int d = distance[i][target];
      if (d == dist) {
        a.add(i);
      } else if (d < dist) {
        n.add(i);
      }
    }
    return new CellRange(a, n);
  }

  /**
   * Gets the row and column coordinates of a cell.
   * @param ix the index of the cell
   * @return the coordinates of the cell
   */
  public int[] getCell(final int ix) {
    return Arrays.copyOf(cell[ix], 2);
  }

  /**
   * Computes the score of a puzzle.
   *
   * Per the puzzle instructions, the score is the sum across rows of the
   * cumulative product of the values in the row.
   *
   * @param solution the solution to evaluate
   * @return the score of the solution
   */
  public int score(final int[][] solution) {
    int sum = 0;
    for (int i = 0; i < dim; i++) {
      sum += Arrays.stream(solution[i])
                   .reduce(1, (product, v) -> product * v);
    }
    return sum;
  }

  /**
   * Gets the set of indices of fixed cells.
   * @return the set of fixed cells
   */
  public Set<Integer> getFixedCells() {
    return new HashSet<>(fixed);
  }

  /**
   * Prints the solution grid to a string.
   * @param solution the solution
   * @param spaces the number of spaces between columns (minimum 1)
   * @param bars if true, lines begin and end with vertical bars
   * @return the formatted solution
   */
  public String printSolution(final int[][] solution, final int spaces,
                              final boolean bars) {
    StringBuilder sb = new StringBuilder();
    // Set the format for each entry based on the number of digits in the
    // largest entry (equal to the maximum block size).
    int digits = (int) Math.floor(Math.log10(maxBlockSize)) + 1;
    int width = Math.max(1, spaces) + digits;
    String fmt = "%" + width + "d";
    String right = String.format("%" + width + "s", "|");
    for (int[] x : solution) {
      if (bars) {
        sb.append("|");
      }
      for (int i : x) {
        sb.append(String.format(fmt, i));
      }
      if (bars) {
        sb.append(right);
      }
      sb.append("\n");
    }
    // Drop the final EOL character.
    sb.deleteCharAt(sb.length() - 1);
    return sb.toString();
  }
}
