package blockparty;

import java.util.Set;

/**
 * CellRange is used to return two sets of cell indices, corresponding to
 * cells at or less than a given distance from a given cell.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public record CellRange(Set<Integer> at, Set<Integer> nearer) { }
