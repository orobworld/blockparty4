package blockparty;

import ilog.concert.IloException;
import ilog.cplex.IloCplex.Status;
import model.CP;
import model.IP;

/**
 * This program tries to solve a number grid puzzle published as the
 * "Block Party 4" puzzle on the Jane Street web site
 * (https://www.janestreet.com/puzzles/archive/).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class BlockParty4 {

  /**
   * Dummy constructor.
   */
  private BlockParty4() { }

  /**
   * Constructs and solves the puzzle.
   *
   * @param args the command line arguments (unused)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Data for the 5x5 example grid.
//    int nb = 8;  // eight blocks
//    int[][] b = new int[][] {
//      {0, 0, 1, 1, 1},
//      {2, 0, 0, 3, 1},
//      {2, 4, 0, 5, 1},
//      {2, 6, 6, 5, 5},
//      {2, 2, 6, 7, 5}
//    };  // block IDs
//    int[][] f = new int[][] {
//      {1, 4, 2},
//      {2, 2, 4},
//      {3, 0, 3}
//    };  // fixed cells
    // Data for the 10 x 10 instance.
    int nb = 23;  // 23 blocks
    int[][] b = new int[][] {
      {0, 1, 1, 1, 2, 2, 2, 2, 2, 2},
      {0, 0, 1, 1, 1, 2, 3, 3, 2, 2},
      {0, 0, 4, 4, 5, 5, 6, 3, 7, 7},
      {0, 0, 8, 4, 9, 6, 6, 6, 7, 7},
      {0, 8, 8, 9, 9, 10, 11, 6, 6, 7},
      {0, 12, 8, 13, 14, 10, 15, 15, 7, 7},
      {0, 16, 17, 13, 13, 13, 15, 18, 18, 18},
      {16, 16, 17, 19, 13, 20, 21, 22, 22, 21},
      {16, 16, 16, 19, 19, 21, 21, 22, 22, 21},
      {16, 16, 16, 16, 19, 19, 21, 21, 21, 21}
    };  // block IDs
    int[][] f = new int[][] {
      {0, 1, 3}, {0, 5, 7}, {1, 3, 4}, {2, 8, 2}, {3, 3, 1}, {4, 0, 6},
      {4, 2, 1}, {5, 7, 3}, {5, 9, 6}, {6, 6, 2}, {7, 1, 2}, {8, 6, 6},
      {9, 4, 5}, {9, 8, 2}
    };  // fixed cells
    // Create the puzzle instance.
    Puzzle puzzle = new Puzzle(nb, b, f);
    // Set a time limit (in seconds) for the solver.
    double tl = 30;
    // Solve using an integer program.
    try (IP ip = new IP(puzzle)) {
      System.out.println("\nUsing the IP model:");
      Status status = ip.solve(tl);
      System.out.println("Final solver status = " + status);
      if (status == Status.Optimal) {
        int[][] grid = ip.getSolution();
        System.out.println("Solution:\n" + puzzle.printSolution(grid, 2, true));
        System.out.println("\nPuzzle score = " + puzzle.score(grid));
      }
    } catch (IloException ex) {
      System.out.println("CPLEX exception:\n" + ex.getMessage());
    }
    // Solve using a CP model.
    try (CP cp = new CP(puzzle)) {
      System.out.println("\nUsing the CP model:");
      if (cp.solve(tl)) {
        int[][] grid = cp.getSolution();
        System.out.println("Solution:\n" + puzzle.printSolution(grid, 2, true));
        System.out.println("\nPuzzle score = " + puzzle.score(grid));
      }
    } catch (IloException ex) {
      System.out.println("CPO exception:\n" + ex.getMessage());
    }
  }

}
