# "Block Party 4" Puzzle #

### What is this repository for? ###

This project contains Java code for integer programming (IP) and constraint programming (CP) models to solve the ["Block Party 4"](https://www.janestreet.com/puzzles/block-party-4-index/) logic puzzle posted by the Jane Street Group.

 Details of both models can be found in my [blog post](https://orinanobworld.blogspot.com/2022/07/block-party-puzzle.html). The code requires both the CPLEX IP solver and the CP Optimizer CP

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

